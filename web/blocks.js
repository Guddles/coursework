const tg = window.Telegram.WebApp;
tg.expand();
// Получаем элементы страницы
const modal = document.getElementById('modal');
const closeBtn = document.querySelector('.close-btn');
const contactForm = document.getElementById('contact-form');
const openBtns = document.querySelectorAll('.open-btn');

// Функция для генерации списка дат и времени
function generateDateTimeList() {
    const datetimeList = document.getElementById('datetime-list');
    const now = new Date();
    const options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit' };
    const barber_id = "1005105493";
    const barber_name = "Евгений";

    for (let i = 0; i < 5; i++) {
        const now = new Date();
        const tommorow = new Date(now.getTime() + 24 * 60 * 60 * 1000);
        const datetime = new Date(tommorow.getTime() + i * 60 * 60 * 1000);
        const formattedDatetime = datetime.toLocaleString('ru-RU', options);
        const li = document.createElement('li');
        li.setAttribute('data-value', formattedDatetime);
        li.setAttribute('data-barber', barber_id)
        li.setAttribute('data-barber_name', barber_name)
        li.textContent = formattedDatetime;
        li.addEventListener('click', function () {
            datetimeList.querySelector('.selected')?.classList.remove('selected');
            this.classList.add('selected');
        });
        datetimeList.appendChild(li);
    }
}

// Добавляем обработчики событий для кнопок открытия модального окна
openBtns.forEach(btn => {
    btn.addEventListener('click', function () {
        const serviceTitle = this.getAttribute('data-service');
        const serviceDescription = this.getAttribute('data-description');
        const serviceDuration = this.getAttribute('data-duration');
        const servicePrice = this.getAttribute('data-price');

        document.getElementById('service-title').textContent = serviceTitle;
        document.getElementById('service-description').textContent = serviceDescription;
        document.getElementById('service-duration').textContent = `Продолжительность: ${serviceDuration}`;
        document.getElementById('service-price').textContent = `Цена: ${servicePrice} руб.`;

        generateDateTimeList();
        modal.style.display = 'block';
    });
});

// Добавляем обработчик событий для кнопки закрытия модального окна
closeBtn.addEventListener('click', function () {
    modal.style.display = 'none';
    const datetimeList = document.getElementById('datetime-list');
    datetimeList.innerHTML = '';
});

// Добавляем обработчик событий для отправки формы
contactForm.addEventListener('submit', function (event) {
    event.preventDefault();
    const name = document.getElementById('name').value;
    const phone = document.getElementById('phone').value;
    const selectedDatetime = document.querySelector('.datetime-list .selected')?.getAttribute('data-value');
    const selectedBarber = document.querySelector('.datetime-list .selected')?.getAttribute('data-barber');
    const selectedBarber_name = document.querySelector('.datetime-list .selected')?.getAttribute('data-barber_name');
    const service = document.getElementById('service-title').textContent
    const description = document.getElementById('service-description').textContent
    console.log(`Имя: ${name}, Барбер_ID: ${selectedBarber}, Имя барбера: ${selectedBarber_name} , Дата и время: ${selectedDatetime}, Телефон: ${phone}`);
    modal.style.display = 'none';
    let data ={
        name: name,
        phone: phone,
        selectedBarber: selectedBarber,
        selectedBarber_name: selectedBarber_name,
        service: service,
        selectedDatetime: selectedDatetime,
        description: description
    }
    
    tg.sendData(JSON.stringify(data));
    tg.close();
});
