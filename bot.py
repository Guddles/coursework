
from aiogram import types
from aiogram.utils import executor
from create_bot import dp, bot
from database import sqlite_db
import pathlib
import sys
import asyncio
import datetime
from handlers import client, barber, check_barb, register_barber, register_service, service_edit, get_web

async def bot_startup(_):
    print("Working now")
    path = pathlib.Path(sys.argv[0]).parent
    print(path)
    sqlite_db.sql_start(path/"database\course.db")


HELP_COMMAND = """
/start - Начать работу с ботом
/help - Вывести список команд
/client - Открыть веб-приложение для записи на услугу
/register - Зарегистрировать себя как поставщика услуги
/edit - Редактирование карточки услуги
/reg - Список услуг, на которые вы записаны
/service - Создать свою карточку услуги
/cancle - Прекратить выполнение текущей команды
"""

@dp.message_handler(commands=["help"])
async def show_help(message: types.Message):
    await message.reply(HELP_COMMAND)

@dp.message_handler(commands=["start"])
async def start_command(message: types.Message):
    await message.delete()
    user_id = message.from_user.id
    await message.answer("Добро пожаловать в Telegram бот, который поможет вам записаться в парикмахерскую")
    asyncio.create_task(check_database(user_id))


async def check_database(user_id):
    while True:
        
        date_from_base = await sqlite_db.check_reg_time(user_id)
        if date_from_base is not None and date_from_base[0] is not None:
            date_from_base = date_from_base[0]
            date_from_base = datetime.datetime.strptime(date_from_base,'%Y-%m-%d %H:%M:%S')
            today = datetime.datetime.today()
            delta = today - date_from_base
            if delta.days <= 1:
                regs = await sqlite_db.get_regs(user_id)
                data = []
                for row in regs:
                    data_str = ", ".join(str(val) for val in row)
                    data.append(data_str)
                data_str = ". ".join(data)
                # отправка сообщения об обнаружении нужной записи
                await bot.send_message(chat_id=user_id, text=f"Приближается время записи {data_str}")
        await asyncio.sleep(86400)



client.register_client_handlers(dp)
# barber.register_barber_handlers(dp)
check_barb.test(dp)
register_barber.register_handlers_barber_reg(dp)
register_service.register_service_handlers(dp)
service_edit.edit_service(dp)
get_web.register_client_webData_handlers(dp)


executor.start_polling(dp, on_startup=bot_startup)
