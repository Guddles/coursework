# from pickle import TUPLE
import sqlite3 as base



def sql_start(path):
    global base, cur, con
    con = base.connect(path)
    cur = con.cursor()
    if con:
        print("OK")
    cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
    print(cur.fetchall())

async def sql_add_barber(state):
    async with state.proxy() as data:
        cur.execute("INSERT INTO barbers (barber_id, first_name, last_name, phone) VALUES(?, ?, ?, ?)", tuple(data.values()))
        con.commit()
        # con.close()
async def sql_add_service(state):
    async with state.proxy() as data:
        cur.execute("INSERT INTO services (service_name, description, price, barber_id) VALUES(?, ?, ?, ?)", tuple(data.values()))
        con.commit()
        # con.close()

async def edit_service(column, new_value, value):
    cur.execute(f"UPDATE services SET {column} = ? WHERE {column} = ? ", (new_value, value))
    con.commit()
    con.close()

async def get_column(column, barber_id):
    cur.execute(f"SELECT DISTINCT {column} FROM services WHERE barber_id = {barber_id}")
    values = [row[0] for row in cur.fetchall()]
    return values

async def sql_add_to_openServices(name, description, barber_name, barber_id, customer_id, reg_time):
    cur.execute(f"INSERT INTO open_services (service_name, description, barber_name, barber_id, customer_id, reg_time) VALUES (?, ?, ?, ?, ?, ?)", (name, description, barber_name, barber_id, customer_id, reg_time))
    con.commit()
    

async def sql_add_to_customers(name, barber_id, reg_time, phone, customer_id):
    cur.execute(f"INSERT INTO customers (first_name, which_barber, reg_time, phone, customer_id) VALUES (?, ?, ?, ?, ?)", (name, barber_id, reg_time, phone, customer_id))
    con.commit()
    # con.close()

async def get_regs(customer_id):
    cur.execute(f"SELECT service_name, description, barber_name, reg_time FROM open_services WHERE customer_id = ?", (customer_id,))
    values = cur.fetchall()
    # con.close()
    return values

async def get_count():
    cur.execute("SELECT COUNT (*) FROM open_services")
    count = cur.fetchone()[0]
    return count

async def check_reg_time(customer_id):
    cur.execute(f"SELECT reg_time FROM open_services WHERE customer_id = {customer_id}" )
    values = cur.fetchone()
    if values:
        return values
    else:
        return

