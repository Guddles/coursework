from create_bot import bot
from aiogram import types, Dispatcher




import sqlite3 as base

async def isBarber(message: types.Message):
    user_id = message.from_user.id
    
    con = base.connect("database\course.db")
    cursor = con.cursor()
    query = "SELECT barber_id FROM barbers WHERE barber_id = ?"
    cursor.execute(query, [user_id])
    result = cursor.fetchone()
    if result is None:
        # await message.answer(f"Вас нет в базе, чтобы зарегестрироавться нажмите -> /register")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        button = types.KeyboardButton(text="/register")
        markup.add(button)
        await bot.send_message(user_id, text="Вас нет в базе, чтобы зарегестрироавться нажмите -> /register", reply_markup=markup)
        return False
        # await bot.send_message(user_id, "/regisrt")
    else:
        query = "SELECT first_name FROM barbers WHERE barber_id = ?"
        cursor.execute(query, [user_id])
        result = cursor.fetchone()
        await message.answer(f"Добро пожаловать {result[0]}")
        return True





def test(dp: Dispatcher):
    dp.register_message_handler(isBarber, commands=["check"])
