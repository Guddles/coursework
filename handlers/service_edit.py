from aiogram import types, Dispatcher
from click import command
from create_bot import bot, dp
from database import sqlite_db
from .check_barb import isBarber
import sqlite3
user_data = {}


@dp.message_handler(commands=["edit"])
async def edit(message: types.Message):
    if await(isBarber(message)):
        markup = types.InlineKeyboardMarkup()
        markup.row(
            types.InlineKeyboardButton(text="Название услуги", callback_data="column:service_name"),
            types.InlineKeyboardButton(text="Описание", callback_data="column:description"),
            types.InlineKeyboardButton(text="Цена", callback_data="column:price")
        )
        await message.reply("Выберите колонку, которую хотите отредактировать:", reply_markup=markup)
    else:
        return



# Обработчик выбора колонки

@dp.callback_query_handler(lambda c: "column:" in c.data)
async def process_column_choice(callback_query: types.CallbackQuery):
    column = callback_query.data.split(":")[1]
    await bot.answer_callback_query(callback_query.id)
    barber_id = callback_query.from_user.id
    values = await sqlite_db.get_column(column, barber_id)
    markup = types.InlineKeyboardMarkup()
    for value in values:
        button = types.InlineKeyboardButton(text=value, callback_data=f"value:{value}")
        markup.add(button)
    user_data[barber_id] = {"column": column}
    await bot.send_message(barber_id, "Выберите какое значение хотите отредактировать:", reply_markup=markup)

@dp.callback_query_handler(lambda c: "value:" in c.data)
async def process_value_choice(callback_query: types.CallbackQuery):
    value = callback_query.data.split(":")[1]
    barber_id = callback_query.from_user.id
    user_data[barber_id]["value"] = value
    await bot.answer_callback_query(callback_query.id)
    await bot.send_message(barber_id, f"Введите новое значение для колонки '{user_data[barber_id]['column']}':")

@dp.message_handler(lambda message: message.from_user.id in user_data and "column" in user_data[message.from_user.id] and "value" in user_data[message.from_user.id])
async def process_new_value(message: types.Message):
    new_value = message.text
    barber_id = message.from_user.id
    column = user_data[barber_id]["column"]
    value = user_data[barber_id]["value"]
    print(column, value, new_value)
    # Обновляем значение в базе данных
    await sqlite_db.edit_service(column, new_value, value)
    

    await message.reply(f"Значение колонки '{column}' для '{value}' успешно обновлено на '{new_value}'.")
    # Удаляем данные пользователя из словаря
    del user_data[barber_id]

def edit_service(dp: Dispatcher):
    dp.register_message_handler(edit, commands=["edit"])
    dp.register_message_handler(process_column_choice)
    dp.register_message_handler(process_value_choice)
    dp.register_message_handler(process_new_value)