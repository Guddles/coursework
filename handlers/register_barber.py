from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from create_bot import bot, dp
from database import sqlite_db
import re
# ID = None

class barbersInfo(StatesGroup):
    barber_id = State()
    first_name = State()
    last_name = State()
    phone = State()

async def first_name(message: types.Message):
    await barbersInfo.first_name.set()
    await message.answer("Если захотите выйти нажмите /cancel")
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    button = types.KeyboardButton(text="/cancel")
    markup.add(button)
    await message.answer("Введите свое имя", reply_markup=markup)
async def cancle_handler(message: types.Message, state: FSMContext):
    current_state = await state.get_state()
    if current_state is None:
        return
    await state.finish()
    await message.answer("Выход")    

async def get_first_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['barber_id'] = message.from_user.id
        data['first_name'] = message.text
    await barbersInfo.next()
    await message.answer("Введите свою фамилию")

async def get_last_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["last_name"] = message.text
    await barbersInfo.next()
    await message.answer("Введите номер телефона включая +7")

async def get_phone(message: types.Message, state: FSMContext):
    if not re.match(r'^\+7\d{10}$', message.text):
        await message.answer("Некорректный номер телефона, попробуйте снова")
        return
    async with state.proxy() as data:
        data["phone"] = message.text
    await message.answer("Спасибо за регистрацию")
    
    await sqlite_db.sql_add_barber(state)
    # async with state.proxy() as data:
    #     await message.reply(str(data))

    await state.finish()


def register_handlers_barber_reg(dp: Dispatcher):
    # dp.register_message_handler(barbers, commands=["register"], state=None)
    # dp.register_message_handler(get_barber_id, state=barbersInfo.barber_id)
    dp.register_message_handler(first_name, commands=["register"], state=None)
    dp.register_message_handler(cancle_handler, state="*", commands=["cancel"])
    dp.register_message_handler(get_first_name, state=barbersInfo.first_name)
    dp.register_message_handler(get_last_name, state=barbersInfo.last_name)
    dp.register_message_handler(get_phone, state=barbersInfo.phone)