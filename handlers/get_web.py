from aiogram import types, Dispatcher
from aiogram.types.web_app_info import WebAppInfo
from datetime import datetime
from database import sqlite_db
import json

async def get_data_from_web(message: types.Message):
    result = json.loads(message.web_app_data.data)
    await message.answer(f'Имя: {result["name"]}. Телефон  {result["phone"]}. Выбранное время: {result["selectedDatetime"]}. Название услуги: {result["service"]}. Имя парикмахера: {result["selectedBarber_name"]}')
    time = result["selectedDatetime"].replace(',', "")
    time2 = datetime.strptime(time, '%d.%m.%Y %H:%M')
    formated = time2.strftime('%Y-%m-%d %H:%M:%S')
    service_name = result["service"]
    description = result["description"]
    barber_name = result["selectedBarber_name"]
    barber_id = result["selectedBarber"]
    phone = result["phone"]
    name = result["name"]
    customer_id = message.from_user.id
    await sqlite_db.sql_add_to_openServices(service_name, description, barber_name, barber_id, customer_id, formated)
    await sqlite_db.sql_add_to_customers(name, barber_id, formated, phone, customer_id)




def register_client_webData_handlers(dp: Dispatcher):
    dp.register_message_handler(get_data_from_web, content_types=['web_app_data'])