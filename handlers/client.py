from aiogram import types, Dispatcher
from create_bot import bot, dp
from aiogram.types.web_app_info import WebAppInfo
from database import sqlite_db



async def open_web_command(message: types.Message):
    # global user_id
    user_id = message.from_user.id
    await message.delete()
    markup = types.ReplyKeyboardMarkup()
    
    markup.add(types.KeyboardButton("Открыть веб-страницу", web_app=WebAppInfo(url="https://couserwork.hostfl.ru/index.html")))
   
    await message.answer("Перейдите на сайт",reply_markup=markup)
    
async def get_reg(message: types.Message):
    await message.answer("Вот ваши записи:")
    regs = await sqlite_db.get_regs(message.from_user.id)
    data = []
    for row in regs:
        data_str = ", ".join(str(val) for val in row)
        data.append(data_str)
    data_str = ". ".join(data)
    await message.answer(data_str)

    




def register_client_handlers(dp: Dispatcher):
    dp.register_message_handler(open_web_command, commands=['client'])
    dp.register_message_handler(get_reg, commands=["reg"])