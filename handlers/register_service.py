from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from create_bot import bot
from database import sqlite_db
from .check_barb import isBarber

class service_info(StatesGroup):
    service_name = State()
    description = State()
    price = State()
    barber_id = ()



async def service_name(message: types.Message):
    if await isBarber(message): 
        await service_info.service_name.set()
        # await message.answer("Если захотите выйти напишите /cancel")
        await message.answer("Если захотите выйти нажмите /cancel")
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        button = types.KeyboardButton(text="/cancel")
        markup.add(button)
        await message.answer("Введите название услуги", reply_markup=markup)
    else: 
        # await isBarber(message)
        return
async def cancle_handler(message: types.Message, state: FSMContext):
    current_state = await state.get_state()
    if current_state is None:
        return
    await state.finish()
    await message.answer("Выход")


async def get_service_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data["service_name"] = message.text
    await service_info.next()
    await message.answer("Напишите описание услуги")



async def get_description(message: types.Message, state:FSMContext):
    async with state.proxy() as data:
        data["description"] = message.text
    await service_info.next()
    await message.answer("Введите цену услуги")

async def get_price(message: types.Message, state: FSMContext):
    valid_price = False
    if not valid_price:
        try:
            price = int(message.text)
            valid_price = True
            async with state.proxy() as data:
                data["price"] = message.text
                data["barber_id"] = message.from_user.id
        except Exception:
            await message.answer("Вы ввели некорректную цену, попробуйте снова")
            return

    await sqlite_db.sql_add_service(state)
    # async with state.proxy() as data:
    #     await message.answer(str(data))
    #     print(data)
    await message.answer("Услуга зарегестрирована")
    await state.finish()

def register_service_handlers(dp: Dispatcher):
    dp.register_message_handler(service_name, commands=["service"], state=None)
    dp.register_message_handler(cancle_handler, state="*", commands=["cancel"])
    dp.register_message_handler(get_service_name, state=service_info.service_name)
    dp.register_message_handler(get_description, state=service_info.description)
    dp.register_message_handler(get_price, state=service_info.price)