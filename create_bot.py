# import aiogram
from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from get_evn import getEnv as getEnv
from aiogram.contrib.fsm_storage.memory import MemoryStorage

TOKEN = getEnv("TOKEN")
bot = Bot(TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
